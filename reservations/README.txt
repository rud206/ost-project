1. When a wants to access the application, he/she is required to sign in using a google account. After signing in the user can create and reserve resources.

2. After signing in the user arrives at the landing page. The landing page has the following information:
	a) List of all Resources on the system(this list is same for all the users)
	b) List of all Resources on the system that the user owns.
	c) List of Reservations made by the user.
	d) A form to create a new Resource

3. On the landing page, a Resource is displayed with the following information:
	a) Resource Name
	b) Start time of resource availability
	c) End time of resource availability
   The Resource name is a link, which redirects the user to the Resource page

4. On the landing page, a Reservation is displayed with the following information:
	a) Name of the Resource that is reserved
	b) Date of the Reservation
	c) Start time of the Reservation
	d) Duration of the Reservation(Duration is always in minutes)
	e) A link to delete the Reservation, since the user is the owner of the Reservation
   The Reservation is deleted when the user presses the delete link

5. On the landing, there is a form for the user to create a new Resource. It takes following information as input:
	a) Name of the resource(this parameter is required)
	b) Availability start time
	c) Availability end time
	d) Tags for the resource(this parameter is optional)
	
6. The Resource page has the following information:
	a) Name of the Resource(Name here is not a link because this is already the Resource page)
	b) Availability start and end times
	c) List of Tags for the resource, each Tag is a link which redirects to the Tag page
	d) List of all Reservation made for this Resource by all users
	e) If the user viewing the Resource page is the owner, then it provides a link to Edit the resource
	f) An RSS Link which displays all the information of the Resource
	g) A Link to reserve the Resource
	h) A Link to go back to the landing page of the user

7. A Reservation on the Resource page has the following information:
	a) User who made the Reservation
	b) Date of Reservation
	c) Start time of Reservation
	d) Duration of Reservation
   The user name is a link which redirects to the user page

8. On the Tag page for a particular tag, the following information is displayed:
	a) A list of Resources which have this particular tag
	b) A link to go back to the landing page for the user
   For the Resource, its name is displayed as a link which redirects to the Resource page. The availability information for the Resource is also displayed
   
9. The Reserve Resource page contains the following information:
	a) The name of the Resource which is a link that redirects to the Resource page
	b) The availability start time of the Resource
	c) The availability end time of the Resource
	d) A form to enter Reservation details, it takes a date, start time and duration for the Reservation as input
	
10. Validation checks for a reservation:
	a) The date of the Reservation should be in the future
	b) The start time of the Reservation should be between the availability start and end time of the Resource
	c) The end time of the Reservation should be between the availability start and end time of the Resource
	d) Start time should be valid and not null
	e) Duration should be greater than 0
	f) The Reservation should not clash with any already existing reservation

11. The User Page has the following information:
	a) List of Resources owned by the user
	b) List of Reservations made by the user