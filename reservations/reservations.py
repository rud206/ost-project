import webapp2
import cgi
import urllib
import jinja2
import os
import uuid
import time
import datetime

from google.appengine.api import users
from google.appengine.ext import ndb

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


DEFAULT_PAGE_NAME = 'default_page'
DEFAULT_KEY = 'default_key'
RDEFAULT_KEY = 'rdefault_key'

def resource_key():
    return ndb.Key('Resource', DEFAULT_KEY)
    
def reservation_key():
    return ndb.Key('Reservation', RDEFAULT_KEY)

class Resource(ndb.Model):
    uid = ndb.StringProperty(indexed=True, required=True)
    owner = ndb.StringProperty(indexed=False)
    starttime = ndb.StringProperty(indexed=False)
    endtime = ndb.StringProperty(indexed=False)
    tags = ndb.StringProperty(repeated=True)
    dateofcreation = ndb.StringProperty()
    name = ndb.StringProperty()   


class Reservation(ndb.Model):
    uid = ndb.StringProperty(indexed=True, required=True)
    resourceuid = ndb.StringProperty(indexed=True)
    resourcename = ndb.StringProperty()
    user = ndb.StringProperty()
    starttime = ndb.StringProperty()
    duration = ndb.StringProperty()
    date = ndb.StringProperty()

    
class CreateResource(webapp2.RequestHandler):
    def post(self):
        stime = self.request.get('starttime')
        etime = self.request.get('endtime')
        strname = self.request.get('nameofresource')
        
        if(len(strname)==0):
            template_values = {
            'error': 'Name is a required field for resource'
            }
            template = JINJA_ENVIRONMENT.get_template('createresourceerror.html')
            self.response.write(template.render(template_values))
            return
        
        try:
            time.strptime(stime,'%H:%M')
        except ValueError:
            template_values = {
            'error': 'Start time is invalid'
            }
            template = JINJA_ENVIRONMENT.get_template('createresourceerror.html')
            self.response.write(template.render(template_values))
            return
        
        try:
            time.strptime(etime,'%H:%M')
        except ValueError:
            template_values = {
            'error': 'End time is invalid'
            }
            template = JINJA_ENVIRONMENT.get_template('createresourceerror.html')
            self.response.write(template.render(template_values))
            return
        
        
        strstime = stime[:2] + stime[3:]
        intstime = int(strstime)
        stretime = etime[:2] + etime[3:]
        intetime = int(stretime)
        
        if(intetime <= intstime):
            template_values = {
            'error': 'End time should be greater than start time'
            }
            template = JINJA_ENVIRONMENT.get_template('createresourceerror.html')
            self.response.write(template.render(template_values))
            return

        page_name = self.request.get('page_name',DEFAULT_PAGE_NAME)
        user = users.get_current_user()
        resource = Resource(parent=resource_key())
        resource.uid = str(uuid.uuid4())
        resource.owner = user.email()
        resource.name = self.request.get('nameofresource')
        resource.starttime = self.request.get('starttime')
        resource.endtime = self.request.get('endtime')
        resource.dateofcreation = time.strftime("%x")
        taglist = {}
        line = self.request.get('tags')
        taglist = line.split(",")
        tlist = []
        for tag in taglist:
            tlist.append(tag.strip())
        resource.tags = tlist
        resource.put()
        self.redirect('/')
        
class ResourceInfo(webapp2.RequestHandler):
    def get(self):
        resources_query = Resource.query(ancestor=resource_key())
        resources = resources_query.fetch()
        rid = self.request.get('value')
        for r in resources:
            if (r.uid==rid):
                rname=r.name
                rstarttime=r.starttime
                rendtime=r.endtime
                rtag = r.tags
                rowner = r.owner
        reservations = Reservation.query().fetch()
        finalreservation = []
        
        for r in reservations:
            if (r.resourceuid == rid):
                finalreservation.append(r)
        user = users.get_current_user().email()
        resource_values = {
            'currentuser': user,
            'uid':rid,
            'name': rname,
            'stime': rstarttime,
            'etime': rendtime,
            'tags': rtag,
            'reservations': finalreservation,
            'resourceowner': rowner
        }
        template = JINJA_ENVIRONMENT.get_template('resource.html')
        self.response.write(template.render(resource_values))
        
class TagInfo(webapp2.RequestHandler):
    def get(self):
        resources_query = Resource.query(ancestor=resource_key())
        resources = resources_query.fetch()
        
        tag = self.request.get('value')
        resourceContainingTag = []
        for r in resources:
            if(tag in r.tags):
                resourceContainingTag.append(r)
        resourceList = {
            'tag': tag,
            'list':resourceContainingTag ,
        }
        template = JINJA_ENVIRONMENT.get_template('tag.html')
        self.response.write(template.render(resourceList))
        
        
class ReserveResource(webapp2.RequestHandler):
    def get(self):
        resources_query = Resource.query(ancestor=resource_key())
        resources = resources_query.fetch()
        id = self.request.get('value')
        user = users.get_current_user().email()
        for r in resources:
            if(r.uid==id):
                name = r.name
                stime = r.starttime
                etime = r.endtime
                dateofcreation = r.dateofcreation
        resource_values = {
            'uid': id,
            'user': user,
            'rname': name,
            'stime': stime,
            'etime': etime,
            'rdateofcreation': dateofcreation,
        }
        template = JINJA_ENVIRONMENT.get_template('reserveresource.html')
        self.response.write(template.render(resource_values))
        

class MakeReservation(webapp2.RequestHandler):
    def post(self):
        
        initialuid = self.request.get('resourceid')
        initialuser = self.request.get('user')
        initialname = self.request.get('resourcename')
        initialstime = self.request.get('avstime')
        initialetime = self.request.get('avetime')
        initialdoc = self.request.get('rdateofcreation')
        reservationstarttime = self.request.get('starttime')
        reservationduration = self.request.get('duration')
        reservationdate = self.request.get('date')
        
        try:
            datetime.datetime.strptime(reservationdate,'%Y-%m-%d')
        except ValueError:
            template_values = {
            'success': 'false',
            'message': 'Date is invalid',
            'resourceid': initialuid,
            }
            template = JINJA_ENVIRONMENT.get_template('displayresult.html')
            self.response.write(template.render(template_values))
            return
        
        
        
        
        
        try:
            time.strptime(reservationstarttime,'%H:%M')
        except ValueError:
            template_values = {
            'success': 'false',
            'message': 'Start time is invalid',
            'resourceid': initialuid,
            }
            template = JINJA_ENVIRONMENT.get_template('displayresult.html')
            self.response.write(template.render(template_values))
            return
        
        intduration = int(reservationduration)
        
        if(intduration < 1):
            template_values = {
            'success': 'false',
            'message': 'Duration should be greater than 0',
            'resourceid': initialuid,
            }
            template = JINJA_ENVIRONMENT.get_template('displayresult.html')
            self.response.write(template.render(template_values))
            return
        
        initialreservationstemp = Reservation.query().fetch()
        initialreservations = []
        
        for res in initialreservationstemp:
            if(res.resourceuid==initialuid):
                initialreservations.append(res)
        
        stringdatereservation = self.request.get('date')
        stringdatewithoutcolon = stringdatereservation[:4] + stringdatereservation[5:7] + stringdatereservation[8:]
        stringdatewithoutcolon = stringdatewithoutcolon[2:]
        intdatewithoutcolon = int(stringdatewithoutcolon)
        
        stringresourcecreationdatewithoutcolon = initialdoc[6:] + initialdoc[:2] + initialdoc[3:5]
        intresourcecreationdate = int(stringresourcecreationdatewithoutcolon)
        
        reservationstarthours = reservationstarttime[:2]
        inthours = int(reservationstarthours) * 60
        
        reservationstartmins = reservationstarttime[3:]
        intmins = int(reservationstartmins)
        
        intstarttimereservation = inthours + intmins
        
        intreservationduration = int(reservationduration)
        
        intendtimereservation = intstarttimereservation + intreservationduration
        
        resourcestarthours = initialstime[:2]
        inthoursresource = int(resourcestarthours) * 60
        
        resourcestartmins = initialstime[3:]
        intminsresource = int(resourcestartmins)
        
        intinitialstime = inthoursresource + intminsresource
        
        resourceendhours = initialetime[:2]
        inthoursresourceend = int(resourceendhours) * 60
        
        resourceendmins = initialetime[3:]
        intminsresourceend = int(resourceendmins)
        
        intinitialetime = inthoursresourceend + intminsresourceend
        
        if(intdatewithoutcolon < intresourcecreationdate):
            template_values = {
            'success': 'false',
            'message': 'You are trying to reserve a resource before its creation',
            'resourceid': initialuid,
            }
            template = JINJA_ENVIRONMENT.get_template('displayresult.html')
            self.response.write(template.render(template_values))
        else:
            if(intstarttimereservation < intinitialstime):
                template_values = {
                'success': 'false',
                'message': 'Start time of reservation should greater than available start time of resource',
                'resourceid': initialuid,
                }
                template = JINJA_ENVIRONMENT.get_template('displayresult.html')
                self.response.write(template.render(template_values))
            else:
                if(intendtimereservation > intinitialetime):
                    template_values = {
                    'success': 'false',
                    'message': 'End time of reservation should less than available end time of resource',
                    'resourceid': initialuid,
                    }
                    template = JINJA_ENVIRONMENT.get_template('displayresult.html')
                    self.response.write(template.render(template_values))
                else:
                    currentdate = time.strftime("%x")
                    stringcurrentdate= currentdate[6:] + currentdate[:2] + currentdate[3:5]
                    intcurrentdate=int(stringcurrentdate)
                    if(intcurrentdate > intdatewithoutcolon):
                        template_values = {
                        'success': 'false',
                        'message': 'Reservation date cannot be in the past',
                        'resourceid': initialuid,
                        }
                        template = JINJA_ENVIRONMENT.get_template('displayresult.html')
                        self.response.write(template.render(template_values))
                    else:
                        if(intendtimereservation > 1440):
                            template_values = {
                            'success': 'false',
                            'message': 'Reservation cannot span across multiple days',
                            'resourceid': initialuid,
                            }
                            template = JINJA_ENVIRONMENT.get_template('displayresult.html')
                            self.response.write(template.render(template_values))
                        else:
                            answer = 'false'
                            for re in initialreservations:
                                restarttime = re.starttime
                                reduration = re.duration
                                restarthours = restarttime[:2]
                                intrestarthours = int(restarthours) * 60
                                restartmins = restarttime[3:]
                                intrestartmins = int(restartmins)
                                restart = intrestarthours + intrestartmins
                                reend = restart + int(reduration)
                                redate = re.date
                                redatetemp = redate[2:4] + redate[5:7] + redate[8:]
                                intredate = int(redatetemp)
                                if(intredate==intdatewithoutcolon):
                                    if(intstarttimereservation >= restart):
                                        if(intstarttimereservation <= reend):
                                            answer = 'true'
                                    if(intendtimereservation >= restart):
                                        if(intendtimereservation <= reend):
                                            answer = 'true'
                            if(answer == 'true'):
                                template_values = {
                                'success': 'false',
                                'message': 'Reservation clashes with an already existing reservation',
                                'resourceid': initialuid,
                                }
                                template = JINJA_ENVIRONMENT.get_template('displayresult.html')
                                self.response.write(template.render(template_values))
                            else:
                                reservation = Reservation(parent=resource_key())
                                reservation.resourceuid = self.request.get('resourceid')
                                reservation.user = self.request.get('user')
                                reservation.date = self.request.get('date')
                                reservation.starttime = self.request.get('starttime')
                                reservation.duration = self.request.get('duration')
                                reservation.resourcename = self.request.get('resourcename')
                                reservation.uid = str(uuid.uuid4())
                                reservation.put()
                                template_values = {
                        '       success': 'true',
                                'message': 'Congrats!! Your reservation was successful.',
                                'resourceid': initialuid,
                                }
                                template = JINJA_ENVIRONMENT.get_template('displayresult.html')
                                self.response.write(template.render(template_values))



class UserInfo(webapp2.RequestHandler):
    def get(self):
        userid = self.request.get('value')
        resources_query = Resource.query(ancestor=resource_key())
        resources = resources_query.fetch()
        reservations = Reservation.query().fetch()
        userresources = []
        userreservations = []
        for s in resources:
            if(s.owner==userid):
                userresources.append(s)
        for r in reservations:
            if(r.user==userid):
                userreservations.append(r)
        template_values = {
        'usertodisplay': userid,
        'resources': userresources,
        'reservations': userreservations
        }
        template = JINJA_ENVIRONMENT.get_template('userinfo.html')
        self.response.write(template.render(template_values))
        

class DeleteReservation(webapp2.RequestHandler):
    def get(self):
        reservationuid = self.request.get('value')
        reservation = Reservation.query(Reservation.uid==reservationuid)
        for r in reservation:
            r.key.delete()
        self.redirect('/')
        
        

class EditResource(webapp2.RequestHandler):
    def get(self):
        resourceuid = self.request.get('value')
        resources_query = Resource.query(ancestor=resource_key())
        resources = resources_query.fetch()
        for r in resources:
            if(r.uid == resourceuid):
                rname = r.name
                rstarttime = r.starttime
                rendtime = r.endtime
                rtags = r.tags
        stringrtags =""
        for t in rtags:
            stringrtags += t + ", "
        stringrtags = stringrtags[:-2]
        template_values = {
        'name': rname,
        'starttime': rstarttime,
        'endtime': rendtime,
        'tags': stringrtags,
        'uid': resourceuid,
        }
        template = JINJA_ENVIRONMENT.get_template('editresource.html')
        self.response.write(template.render(template_values))


class ModifyResource(webapp2.RequestHandler):
    def post(self):
        resourceuid = self.request.get('uid')
        resources = Resource.query(Resource.uid == resourceuid).fetch()
        for resource in resources:
            resource.name = self.request.get('nameofresource')
            resource.starttime = self.request.get('starttime')
            resource.endtime = self.request.get('endtime')
            taglist = {}
            line = self.request.get('tags')
            taglist = line.split(",")
            tlist = []
            for tag in taglist:
                tlist.append(tag.strip())
            resource.tags = tlist
            resource.put()
        self.redirect('/')
        

class GenerateRSS(webapp2.RequestHandler):
    def get(self):
        resourceuid = self.request.get('value')
        reservations = Reservation.query().fetch()
        resources_query = Resource.query(ancestor=resource_key())
        resources = resources_query.fetch()
        
        for r in resources:
            if(r.uid==resourceuid):
                rname = r.name
                rowner = r.owner
                rtags = r.tags
        
        rlist = []
        for r in reservations:
            if(r.resourceuid==resourceuid):
                rlist.append(r)
        template_values = {
        'name': rname,
        'owner': rowner,
        'tags': rtags,
        'list': rlist,
        }
        template = JINJA_ENVIRONMENT.get_template('generaterss.html')
        self.response.write(template.render(template_values))
        

class MainPage(webapp2.RequestHandler):

    def get(self):
        page_name = self.request.get('page_name', DEFAULT_PAGE_NAME)
        resources_query = Resource.query(ancestor=resource_key())
        resources = resources_query.fetch()
        user = users.get_current_user()
        if user:
            user = users.get_current_user().email()
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
            reservations = Reservation.query().fetch()
            userreservations = []
            for r in reservations:
                if(r.user==user):
                    userreservations.append(r)
            template_values = {
            'user': user,
            'resource': resources,
            'page_name': urllib.quote_plus(page_name),
            'url': url,
            'url_linktext': url_linktext,
            'reservations': userreservations,
            'error':'false'
            }
            template = JINJA_ENVIRONMENT.get_template('index.html')
            self.response.write(template.render(template_values))
        else:
            self.redirect(users.create_login_url(self.request.uri))
            


        
        
app = webapp2.WSGIApplication([('/', MainPage),
('/create', CreateResource),
('/resource', ResourceInfo),
('/tags', TagInfo),
('/reserve',ReserveResource),
('/makereservation',MakeReservation),
('/userinfo',UserInfo),
('/deletereservation',DeleteReservation),
('/editresource',EditResource),
('/modify',ModifyResource),
('/generaterss',GenerateRSS)
], debug=True)